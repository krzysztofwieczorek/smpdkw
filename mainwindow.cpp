#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QImage>
#include <QDebug>




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    FSupdateButtonState();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDatabaseInfo()
{
    ui->FScomboBox->clear();
    for(unsigned int i=1; i<=database.getNoFeatures(); ++i)
        ui->FScomboBox->addItem(QString::number(i));

    ui->FStextBrowserDatabaseInfo->setText("noClass: " +  QString::number(database.getNoClass()));
    ui->FStextBrowserDatabaseInfo->append("noObjects: "  +  QString::number(database.getNoObjects()));
    ui->FStextBrowserDatabaseInfo->append("noFeatures: "  +  QString::number(database.getNoFeatures()));

}

void MainWindow::FSupdateButtonState(void)
{
    if(database.getNoObjects()==0)
    {
        FSsetButtonState(false);
    }
    else
        FSsetButtonState(true);

}


void MainWindow::FSsetButtonState(bool state)
{
    ui->FScomboBox->setEnabled(state);
    ui->FSpushButtonCompute->setEnabled(state);
    ui->FSpushButtonSaveFile->setEnabled(state);
    ui->FSradioButtonFisher->setEnabled(state);
    ui->FSradioButtonSFS->setEnabled(state);
}

void MainWindow::on_FSpushButtonOpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open TextFile"), "", tr("Texts Files (*.txt)"));

    if ( !database.load(fileName.toStdString()) )
        QMessageBox::warning(this, "Warning", "File corrupted or not loaded!");
    else
        QMessageBox::information(this, fileName, "File loaded!");

    FSupdateButtonState();
    updateDatabaseInfo();
}

int MainWindow::factorial(int i){
    if (i == 0) return 1;
    else return i*factorial(i-1);
}
// n - size of A or Q, k - dim
int MainWindow::combinations(int n, int k){
    return factorial(n)/factorial(k)*factorial(n-k);
}

//std::vector<std::pair<int,int>> MainWindow::combine(int size){
//    std::vector<std::pair<int,int>> combinations;
//    for(int i=1;i<=size;i++)
//                 for (int j=i+1;j<=size;j++)
//                    if (i!=j){
//                        combinations.push_back(std::make_pair(i,j));
//                        qDebug() << QString::number(i) << " " << QString::number(j);
//                    }
//    return combinations;
//}

/**
 * Celem tej metody jest ustalenie największego współczynnika Fishera, który stanowi o separowalności cech.
 * Dla wymiaru równego 1, można zapisać go w uproszczeniu. Natomiast dla większej liczby wymiarów należy wykonać proste operacje macierzowe.
 * @brief MainWindow::on_FSpushButtonCompute_clicked
 */
void MainWindow::on_FSpushButtonCompute_clicked()
{
    int dimension = ui->FScomboBox->currentText().toInt();


    if( ui->FSradioButtonFisher ->isChecked())
    {
        if (dimension == 1 && database.getNoClass() == 2)
        {
            float FLD = 0, tmp;
            int max_ind = -1;

            //std::map<std::string, int> classNames = database.getClassNames();
            for (uint i = 0; i < database.getNoFeatures(); ++i)
            {
                std::map<std::string, float> classAverages;
                std::map<std::string, float> classStds;

                for (auto const &ob : database.getObjects())
                {
                    classAverages[ob.getClassName()] += ob.getFeatures()[i];
                    classStds[ob.getClassName()] += ob.getFeatures()[i] * ob.getFeatures()[i];
                }

                std::for_each(database.getClassCounters().begin(), database.getClassCounters().end(), [&](const std::pair<std::string, int> &it)
                {
                    classAverages[it.first] /= it.second;
                    classStds[it.first] = std::sqrt(classStds[it.first] / it.second - classAverages[it.first] * classAverages[it.first]);
                }
                );

                tmp = std::abs(classAverages[ database.getClassNames()[0] ] - classAverages[database.getClassNames()[1]]) / (classStds[database.getClassNames()[0]] + classStds[database.getClassNames()[1]]);

                if (tmp > FLD)
                {
                    FLD = tmp;
                    max_ind = i;
                }

            }

            ui->FStextBrowserDatabaseInfo->append("max_ind: "  +  QString::number(max_ind) + " " + QString::number(FLD));
        }
        else{
            //combine(database.getNoObjects());
            std::vector<float> fishers;
            boost::numeric::ublas::matrix<float> sA (dimension, dimension);
            boost::numeric::ublas::matrix<float> sB (dimension, dimension);
            boost::numeric::ublas::matrix<float> mA (1, dimension);
            boost::numeric::ublas::matrix<float> mB (1, dimension);
            int iterA = 0, iterB = 0;
            int divline = 0;

            for(int i = 0; i<database.getObjects().size(); i++){
                if(database.getObjects().at(i).getClassName() == QUERCUS){
                    divline = i;
                    break;
                }
            }

            for (int i = 0; i<divline; i++){ //dla kazdego Acer
                for (int k = 0; k < dimension; k++){
                    sA(iterA,k) = database.getObjects().at(i).getFeatures().at(k);
                }

                for (int j = divline; j<database.getObjects().size(); j++){ //dla kazdego Quercus
                    for (int k = 0; k < dimension; k++){
                        sB(iterB,k) = database.getObjects().at(j).getFeatures().at(k);
                    }
                    iterB++;
                }

//                for(int j=0; j<dimension; j++){
//                    for(int k=0; k<database.getNoFeatures(); k++){
//                        mA(1,j) += sA(j,k);
//                        mB(1,j) += sB(j,k);
//                    }
//                    mA(1,j) /= database.getNoFeatures();
//                    mB(1,j) /= database.getNoFeatures();
//                    sA = sA-mA;
//                    sB = sB-mB;
//                }

                iterB = 0;
                iterA++;
            }

//            std::string bitmaskA(dimension, 1); // K leading 1's
//            bitmaskA.resize(divline, 0); // N-K trailing 0's


//            std::string bitmaskB(dimension, 1); // K leading 1's
//            bitmaskB.resize(database.getObjects().size()-divline, 0); // N-K trailing 0's

//            do {
//                for (int i = 0; i < divline; ++i) // [0..N-1] integers
//                {
//                    if (bitmaskA[i]){
//                        //qDebug() << i;
//                        for(int j=0; j<database.getNoFeatures(); j++){
//                            sA(j,k) = database.getObjects().at(k);
//                        }

//                    do {
//                        for (int j = divline; j < database.getObjects().size()-divline; ++j) // [0..N-1] integers
//                        {
//                            if (bitmaskB[j]) qDebug() << j;
//                        }
//                        qDebug() << "-------------||||||||||||--------------";
//                    } while (std::prev_permutation(bitmaskB.begin(), bitmaskB.end()));
//                    }

//                }
//                qDebug() << "---------------------------";
//            } while (std::prev_permutation(bitmaskA.begin(), bitmaskA.end()));

//            for(int i=0; i<combinations(divline-1, dimension)*combinations(database.getObjects().size()-divline-1, dimension); i++){
//                //aquisite a
//                //to jest glupota :)
//                for(int j=iterA; j<dimension; j++){
//                    for(int k = 0; k < database.getNoFeatures(); k++){
//                        sA(j,k) = database.getObjects().at(k);
//                    }
//                    iterA++;
//                }
//                //aquisite b

//                for(int j=iterB; j<dimension; j++){
//                    for(int k = 0; k < database.getNoFeatures(); k++){
//                        sA(j,k) = database.getObjects().at(k);
//                    }
//                    iterA++;
//                }

//                //compute ma & mb
//                for(int j=0; j<dimension; j++){
//                    for(int k=0; k<database.getNoFeatures(); k++){
//                        mA(1,j) += sA(j,k);
//                        mB(1,j) += sB(j,k);
//                    }
//                    mA(1,j) /= database.getNoFeatures();
//                    mB(1,j) /= database.getNoFeatures();
//                    sA = sA-mA;
//                    sB = sB-mB;
//                }
//                //compute fisher
//            }

        }
    }
}



void MainWindow::on_FSpushButtonSaveFile_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Open TextFile"), "D:\\Users\\Krzysiu\\Documents\\Visual Studio 2015\\Projects\\SMPD\\SMPD\\Debug\\", tr("Texts Files (*.txt)"));
    QMessageBox::information(this, "My File", fileName);
    database.save(fileName.toStdString());
}

void MainWindow::on_PpushButtonSelectFolder_clicked()
{

}

void MainWindow::CTabUpdate(){
    const QStringList clfs = QStringList() << QString("(k)NN") << QString("(k)NM") ;
    ui->CcomboBoxClassifiers->addItems(clfs);
    const QStringList kList = QStringList() << QString("1") << QString("3") << QString("5")
                                            << QString("7") << QString("9") << QString("11");
    ui->CcomboBoxK->addItems(kList);
    ui->CPart->setValidator(new QRegExpValidator(QRegExp("[1-9][0-9][0]")));
}

void MainWindow::on_CpushButtonOpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open TextFile"), "", tr("Texts Files (*.txt)"));

    if ( !database.load(fileName.toStdString()) )
        ui->CtextBrowser->append("File couldn't be opened correctly");
    else {
        ui->CtextBrowser->append("File loaded. Please fill the form.");
        CTabUpdate();

    }
}

void MainWindow::on_CpushButtonSaveFile_clicked()
{

}

/**
 * @brief MainWindow::on_CpushButtonTrain_clicked
 */
void MainWindow::on_CpushButtonTrain_clicked()
{
    //TODO: pobieranie k musi być zależne od metody
    int k;
    if(ui->CPart->text().length() != 0) k = ui->CcomboBoxK->currentText().toInt();
    else { ui->CtextBrowser->append(QString("Please fill 'Training Part' field")); return; }

    srand(time(NULL));
    std::pair<Database,Database> trainTestPair = divideSets();

    //kNN
    /**
      1. Wybieramy punkt losowy sposród zbioru treningowego.
      2. Obliczaj odległość między punktem losowym a każdym kolejnym ze zbioru testowego.
      3. Jeżeli odległość jest < od któregokolwiek z wcześniej zapisanych usuń ostatni i dodaj ten w miejscu.
      */
    if(ui->CcomboBoxClassifiers->currentText() == "(k)NN"){
        //ui->CtextBrowser->clear();
        std::vector<std::pair<double, Object>> kNeighbours;

        int acer = 0, quercus = 0;
        // 1.
        Object randObject = trainTestPair.first.getObjects()
                .at(rand()%trainTestPair.first.getObjects().size());
        ui->CtextBrowser->append(QString("Rand Object class is: %1").arg(QString::fromStdString(randObject.getClassName())));

        for(Object o : trainTestPair.first.getObjects()){
            float dist = 0;
            // 2.
            for(int i = 0; i < o.getFeatures().size(); i++){
                dist += std::pow((randObject.getFeatures().at(i) - o.getFeatures().at(i)), 2);
            }
            float _dist = std::sqrt(dist);
            if(_dist == 0) continue;
            //qDebug() << _dist;
            if (kNeighbours.size() >= k)
                for(int i=0; i<kNeighbours.size(); i++){
                    // 3.
                    if(_dist < kNeighbours.at(i).first){
                        kNeighbours.pop_back();
                        kNeighbours.insert(kNeighbours.begin()+i, std::make_pair(_dist, o));
                        break;
                    }
                }
            // 4.
            else{
                kNeighbours.push_back(std::make_pair(_dist, o));
            }
        }
        for(int i=0; i<kNeighbours.size(); i++){
            ui->CtextBrowser->append(QString("Dist: " + QString::number(kNeighbours.at(i).first) + " %1")
                                     .arg(QString::fromStdString(kNeighbours.at(i).second.getClassName())));
            if(kNeighbours.at(i).second.getClassName() == ACER) acer++;
            else quercus++;
        }

        if(acer > quercus)
            ui->CtextBrowser->append(QString("Klasa obiektu testowego: %1, zaklasyfikowano do Acer")
                                     .arg(QString::fromStdString(randObject.getClassName())));
        else
            ui->CtextBrowser->append(QString("Klasa obiektu testowego: %1, zaklasyfikowano do Quercus")
                                     .arg(QString::fromStdString(randObject.getClassName())));
        //qDebug() << "Acer|Quercus" << acer << "|" << quercus;
    }

    /**

    1.Ustalamy liczbę skupień.
    Jedną z metod ustalenia ilości skupień jest umowny jej wybór i ewentualna późniejsza zmiana tej liczby w celu uzyskania lepszych wyników. Wybór liczby skupień może być oparty również na wynikach innych analiz.
    2.Ustalamy wstępne środki skupień.
    Środki skupień tak zwane centroidy możemy dobrać na kilka sposobów: losowy wybór k obserwacji, wybór k pierwszych obserwacji, dobór w taki sposób, aby zmaksymalizować odległości skupień. Jedną z najczęściej stosowanych metod jest kilkakrotne uruchomienie algorytmu i wybór najlepszego modelu, gdy wstępnie środki skupień były wybierane losowo.
    3.Obliczamy odległości obiektów od środków skupień.
    Wybór metryki jest bardzo istotnym etapem w algorytmie. Wpływa ona na to, które z obserwacji będą uważane za podobne, a które za zbyt różniące się od siebie. Najczęściej stosowaną odległością jest odległość euklidesowa. Stosuje się również kwadrat tej odległości czy też odległość Czebyszewa.
    4.Przypisujemy obiekty do skupień
    Dla danej obserwacji porównujemy odległości od wszystkich skupień i przypisujemy ją do skupienia, do którego środka ma najbliżej.
    5.Ustalamy nowe środki skupień
    Najczęściej nowym środkiem skupienia jest punkt, którego współrzędne są średnią arytmetyczną współrzędnych punktów należących do danego skupienia.
    6.Wykonujemy kroki 3,4,5 do czasu, aż warunek zatrzymania zostanie spełniony.
    Najczęściej stosowanym warunkiem stopu jest ilość iteracji zadana na początku lub brak przesunięć obiektów pomiędzy skupieniami.

      */
    //kNM
    else{
        //ui->CtextBrowser->clear();
        // 1.
        // 2.
        std::vector<std::pair<int, Object>> centers;
        bool endCondition = false;

        for(int i=0; i<k; i++){
            int index = rand()%trainTestPair.first.getObjects().size();
            for(int i=0; i<centers.size(); i++)
                if(centers.at(i).first == index) {i--; continue;}
            centers.push_back(std::make_pair(index, trainTestPair.first.getObjects().at(index)));
        }
        //3,4,5
        do{
            for(int i=0; i<centers.size(); i++){
                for(auto test : trainTestPair.first.getObjects()){
                    float distance = getDistance(centers.at(i).second.getFeatures(), test.getFeatures());
                    qDebug() << QString::number(distance);
                    return;
                }
            }
        }
        while(endCondition);
    }
}



void MainWindow::on_CpushButtonExecute_clicked()
{

}

void MainWindow::errorMsg(QString msg){
    QMessageBox messageBox;
    messageBox.critical(0, "Error", msg);
    messageBox.setFixedSize(500, 200);
}

/**
 * @brief MainWindow::getDistance
 * @param v1 feature vector1
 * @param v2 feature vector2
 * @return distance between vector1 and vector2
 */
float MainWindow::getDistance(std::vector<float> v1, std::vector<float> v2){
    float distance = 0;
    for(auto f1 : v1)
        for(auto f2 : v2){
            distance += std::pow((f1-f2), 2);
        }
    return std::sqrt(distance);
}

/**
 * @brief MainWindow::divideSets
 * @return pair<Database, Database> divided for test and training set of features
 */
std::pair<Database, Database> MainWindow::divideSets(){
    Database trainingSet, testSet;
    int acer = 0, quercus = 0;
    srand(time(NULL));
    for(int i=0; i<database.getNoObjects(); i++){
        if(rand()%100 < ui->CPart->text().toInt()){
            trainingSet.addObject(database.getObjects().at(i));
            if(database.getObjects().at(i).getClassName() == ACER) acer++;
            else quercus++;
        }
        else
            testSet.addObject(database.getObjects().at(i));
    }
    ui->CtextBrowser->clear();
    ui->CtextBrowser->append(QString("Training Features = %1, Test Features = %2 Acer's = %3 Quercus's = %4")
                             .arg(trainingSet.getObjects().size())
                             .arg(testSet.getObjects().size())
                             .arg(QString::number(acer))
                             .arg(QString::number(quercus)));
    return std::make_pair(trainingSet, testSet);
}
