#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define ACER "Acer"
#define QUERCUS "Quercus"

#include <QMainWindow>
#include <fstream>
#include <QCheckBox>
#include <QFileDialog>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <algorithm>

#include "matrixutil.hpp"
#include"database.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void errorMsg(QString msg);

private:
    bool loadFile(const std::string &fileName);
    void updateDatabaseInfo();
    void saveFile(const std::string &fileName);

    void FSupdateButtonState(void);
    void FSsetButtonState(bool state);

    void CTabUpdate();

    std::pair<Database, Database> divideSets();
    float getDistance(std::vector<float> v1, std::vector<float> v2);
    //std::vector<std::pair<std::pair<int,int>>> combine(int size);
    int combinations(int n, int k);
    int factorial(int n);


private slots:
    void on_FSpushButtonOpenFile_clicked();

    void on_FSpushButtonCompute_clicked();

    void on_FSpushButtonSaveFile_clicked();

    void on_PpushButtonSelectFolder_clicked();


    void on_CpushButtonOpenFile_clicked();

    void on_CpushButtonSaveFile_clicked();

    void on_CpushButtonTrain_clicked();

    void on_CpushButtonExecute_clicked();

private:
    Ui::MainWindow *ui;

private:
     Database database;
};

#endif // MAINWINDOW_H
